	$(document).ready(function() {
		/* comprobamos ancho de navegador */
		var ventana_ancho = $(window).width();
		/* Dependiendo del ancho del navegador el background-position sera diferente */
		if(ventana_ancho>769){
			$(window).scroll(function(){
				var barra = $(window).scrollTop();
				var posicion = barra *0.50;

				$(".cabecera").css({
					'background-position': '0 -'+posicion+'px'
				});
			});
		}
			else if(ventana_ancho<769){
				$(window).scroll(function(){
					var barra = $(window).scrollTop();
					var posicion = barra *0.50;

					$(".cabecera").css({
						'background-position': '-400px -'+posicion+'px'
					});
				});
			}

		$(".imagenPerfil").mouseover(function(){
			$(this).css({
				'filter':'sepia(0%)'
			});
		});

		$(".imagenPerfil").mouseout(function(){
			$(this).css({
				'filter':'sepia(90%)'
			});
		});

		$(".ocultar").click(function(){
			$(".imagenPerfil").fadeOut(1000);
			$(".nombre").fadeOut(1000);
			$(".developer").fadeOut(1000);
			$(".lista").fadeOut(1000);
			$(".mostrar").css({
				'display':'block'
			});
			$(".ocultar").css({
				'display':'none'
			});
		});

		$(".mostrar").click(function(){
			$(".imagenPerfil").fadeIn(2000);
			$(".nombre").fadeIn(2000);
			$(".developer").fadeIn(2000);
			$(".lista").fadeIn(2000);
			$(".ocultar").css({
				'display':'block'
			});
			$(".mostrar").css({
				'display':'none'
			});
		});

		setInterval("rotacion()",3000);

		/* Funcion para que la flecha del header vaya hasta el primer div del menu
		principal */
		$(".accionIrAmain").click(function(){
			/* Vamos a guardar en la variable posicion, el valor en pixeles del header */
			var elemento = $(".container-fluid");
			var posicion = elemento.position().top;
			/* Con esta funcion lo que hacemos es crear una animacion de desplazamiento
			hacia el div que viene a continuacion del header sin que el usuario tenga
			que hacer scroll */
			$('html,body').animate({
		        scrollTop: posicion
		    }, 2000);
		});
	

	});

	/* Funcion para crear numero random */
	jQuery.Random = function(m,n) {
        m = parseInt(m);
        n = parseInt(n);
        return Math.floor( Math.random() * (n - m + 1) ) + m;
    }

    var totalRotacion=0;
    var ran = 0;

    /* Funcion donde se creara la rotacion con ciertos grados que despues se llamara 
    por setInterval de modo random*/
    function rotacion() 
	{
	    var vector = [90,180,270,360];
				
		var random = $.Random(0,3);

		var i = 0;

		/* Comprobacion del valor random para evitar que se repita y la flecha no se
		mueva */
		if(ran==random && random<3){
			random = random+1;
		}
		else if(ran==random && random==3){
			random = random-1;
		}

		else{
			random = random;
		}
		/* Aqui acaba la comprobacion del random */
					
		var valor = vector[random];

		totalRotacion = totalRotacion+valor;

		$("#brujula").addClass("girando");
		$(".girando").css({
			'-moz-transform':'rotate('+totalRotacion+'deg)',
			'-webkit-transform':'rotate('+totalRotacion+'deg)'
		});

		ran = random;

		totalRotacion = 0;

		/* Añadimos la clase listaMenuTransform a ciertos items dependiendo del 
		valor de la rotacion */
		if(valor==90){
			$("#1,#3,#5").addClass("listaMenuTransform");
			$("#2,#4,#6").removeClass("listaMenuTransform");
			$("#9").removeClass("listaMenuTransform");
			$("#11,#13").removeClass("listaMenuTransform");
		}
		else if(valor==180){
			$("#2,#4,#6").addClass("listaMenuTransform");
			$("#1,#3,#5").removeClass("listaMenuTransform");
			$("#9").removeClass("listaMenuTransform");
			$("#11,#13").removeClass("listaMenuTransform");
		}
		else if(valor==270){
			$("#9").addClass("listaMenuTransform");
			$("#1,#3,#5").removeClass("listaMenuTransform");
			$("#2,#4,#6").removeClass("listaMenuTransform");
			$("#11,#13").removeClass("listaMenuTransform");
		}
		else if(valor==360){
			$("#11,#13").addClass("listaMenuTransform");
			$("#1,#3,#5").removeClass("listaMenuTransform");
			$("#2,#4,#6").removeClass("listaMenuTransform");
			$("#9").removeClass("listaMenuTransform");
		}

	};